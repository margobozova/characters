import React from 'react';
import './style.css'
import PropTypes from 'prop-types';

export default function PersonList({ handleOpen, characters }) {
    return (
        <ul className={'list'}>
            {characters.map((person) => (
              <li
                // onClick={() => handleOpen(person.name)} // такое оглашение функции влечет за собой ререндер элемента
                // эта функция пересоздается каждый раз
                key={person.name}
                onClick={handleOpen(person.name)}

                // можно написать так, как написано выше, но при условии, что функиця handleOpen написана как:
                // handleOpen = (name) => (тут может быть event)) => { ... }
                //
              >
                  {person.name}
                  {console.log(person.name)}
              </li>
            ))}
        </ul>
    )
}

PersonList.propTypes = {
    handleOpen: PropTypes.func,
    characters: PropTypes.array
};
